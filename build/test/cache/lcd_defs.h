





typedef void (*command_ptr)(uint8_t byte);

typedef void (*data_ptr)(char byte);

typedef void (*delay_ptr)(uint16_t delay);



struct char_lcd_dev {



    uint8_t entry_mode;

    uint8_t display_control;

    uint8_t display_shift;

    uint8_t function_set;



    data_ptr lcd_send_databyte;

    command_ptr lcd_send_commandbyte;

    delay_ptr delay_useconds;

    delay_ptr delay_mseconds;



};

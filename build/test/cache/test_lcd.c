#include "build/temp/_test_lcd.c"
#include "mock_bsp.h"
#include "lcd_defs.h"
#include "lcd.h"
#include "unity.h"




struct char_lcd_dev *dut_struct;



void setUp(void)

{

    dut_struct = malloc(sizeof(struct char_lcd_dev));

    dut_struct->function_set = (0x20 | (0 << 4) | (0 << 2) | (1 << 3) );

    dut_struct->entry_mode = 0x04 | (1 << 1) | (1);

    dut_struct->display_control = 0x08 | (1 << 2) | (1 << 1) | (1);

    dut_struct->display_shift = 0x10 | (0 << 3) | (1 << 2 );

    dut_struct->lcd_send_commandbyte = lcd_command;

    dut_struct->lcd_send_databyte = lcd_data;

    dut_struct->delay_mseconds = delay;

    dut_struct->delay_useconds = delay;

}



void tearDown(void)

{

    free(dut_struct);

}





void test_lcdinitcallsfunctionsinorder(void)

{



    lcd_command_CMockExpect(32, 0x28);

    delay_CMockExpect(33, 5);

    lcd_command_CMockExpect(34, 0x28);

    delay_CMockExpect(35, 100);

    lcd_command_CMockExpect(36, 0x28);

    delay_CMockExpect(37, 100);

    lcd_command_CMockExpect(38, 0x28);

    delay_CMockExpect(39, 100);

    lcd_command_CMockExpect(40, 0);

    lcd_command_CMockIgnoreArg_byte(41);

    delay_CMockExpect(42, 100);



    printf("Function_set: %#03x\n", dut_struct->function_set);





    lcd_init(dut_struct);



}

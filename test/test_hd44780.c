#include "unity.h"
#include "hd44780.h"
#include "lcd_defs.h"
#include "stdlib.h"
#include "stdbool.h"
#include "mock_bsp.h"

void setUp(void)
{
}

void tearDown(void)
{
}

// void test_hd44780_initfailswhenEntryModeisgreaterthanEntryModeBase(void)
// {

//     struct char_lcd_dev *dut_struct = malloc(sizeof(struct char_lcd_dev));
//     dut_struct->entry_mode = ENTRY_MODE_BASE;

//     int8_t return_code = lcd_init(dut_struct);

//     TEST_ASSERT_EQUAL_INT(-1, return_code);
// }

// void test_hd44780_initsucceedswhenEntryModeislessthanEntryModeBase(void)
// {

//     struct char_lcd_dev *dut_struct = malloc(sizeof(struct char_lcd_dev));
//     dut_struct->entry_mode = ENTRY_MODE_BASE - 1;

//     int8_t return_code = lcd_init(dut_struct);

//     TEST_ASSERT_EQUAL_INT(0, return_code);
// }

// void test_hd44780_initfailswhenDisplayControlislessthanDisplayControlBase(void)
// {

//     struct char_lcd_dev *dut_struct = malloc(sizeof(struct char_lcd_dev));
//     dut_struct->display_control = DISPLAY_CONTROL_BASE;

//     int8_t return_code = lcd_init(dut_struct);

//     TEST_ASSERT_EQUAL_INT(-1, return_code);
// }

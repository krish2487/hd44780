#include "unity.h"
#include "lcd.h"
#include "lcd_defs.h"
#include "stdlib.h"
#include "stdbool.h"
#include "mock_bsp.h"

struct char_lcd_dev *dut_struct;

void setUp(void)
{
    dut_struct = malloc(sizeof(struct char_lcd_dev));
    dut_struct->function_set = (FUNCTION_SET_BASE | FUNCTION_SET_DL_4BIT | FUNCTION_SET_5x8 | FUNCTION_SET_2_LINE );
    dut_struct->entry_mode = ENTRY_MODE_BASE | ENTRY_MODE_INCREMENT | ENTRY_MODE_DISPLAYSHIFT;
    dut_struct->display_control = DISPLAY_CONTROL_BASE | DISPLAY_CONTROL_ON | DISPLAY_CONTROL_CURSORON | DISPLAY_CONTROL_BLINK;
    dut_struct->display_shift = DISPLAY_SHIFT_BASE | DISPLAY_SHIFT_CURSOR | DISPLAY_SHIFT_RIGHT;
    dut_struct->lcd_send_commandbyte = lcd_command;
    dut_struct->lcd_send_databyte = lcd_data;
    dut_struct->delay_mseconds = delay;
    dut_struct->delay_useconds = delay;
}

void tearDown(void)
{
    free(dut_struct);
}


void test_lcdinitcallsfunctionsinorder(void)
{

    lcd_command_Expect(0x28);
    delay_Expect(5);
    lcd_command_Expect(0x28);
    delay_Expect(100);
    lcd_command_Expect(0x28);
    delay_Expect(100);
    lcd_command_Expect(0x28);
    delay_Expect(100);
    lcd_command_Expect(0);
    lcd_command_IgnoreArg_byte();
    delay_Expect(100);

    printf("Function_set: %#03x\n", dut_struct->function_set);


    lcd_init(dut_struct);
    // TEST_IGNORE_MESSAGE("Ignore me");
}

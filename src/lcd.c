
#include "lcd.h"
#include "assert.h"

struct char_lcd_dev *init_lcd_struct(uint8_t entry_mode, uint8_t display_control, uint8_t function_set, uint8_t display_shift, data_ptr send_data, command_ptr send_command, delay_ptr milliseconds, delay_ptr microseconds)
{

    struct char_lcd_dev *structure = malloc(sizeof(struct char_lcd_dev));

    structure->entry_mode = ENTRY_MODE_BASE | entry_mode;
    structure->display_control = DISPLAY_CONTROL_BASE | display_control;
    structure->function_set = FUNCTION_SET_BASE | function_set;
    structure->display_shift = DISPLAY_SHIFT_BASE | display_shift;
    structure->lcd_send_commandbyte = send_command;
    structure->lcd_send_databyte = send_data;
    structure->delay_useconds = microseconds;
    structure->delay_mseconds = milliseconds;
    return structure;
}

void lcd_send_data(char *data, uint8_t length, struct char_lcd_dev *lcd_device_struct)
{
    for (uint8_t i = 0; i <= length - 1 ; i++)
        {
            lcd_device_struct->lcd_send_databyte(*data++);
        }
}


int lcd_init(struct char_lcd_dev *lcd_device_struct)
{

    lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->function_set);
    lcd_device_struct->delay_mseconds(5);
    lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->function_set);
    lcd_device_struct->delay_useconds(100);
    lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->function_set);
    lcd_device_struct->delay_useconds(100);
    lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->function_set);
    lcd_device_struct->delay_useconds(100);

    lcd_device_struct->lcd_send_commandbyte(DISPLAY_CONTROL_BASE | DISPLAY_CONTROL_OFF);
    lcd_device_struct->delay_mseconds(100);

    // lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->entry_mode);
    // lcd_device_struct->delay_useconds(100);

    // lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->display_shift);
    // lcd_device_struct->delay_useconds(100);

    // lcd_device_struct->lcd_send_commandbyte(lcd_device_struct->display_control);
    // lcd_device_struct->delay_mseconds(100);

    // lcd_device_struct->lcd_send_commandbyte(RETURN_HOME);
    // lcd_device_struct->delay_mseconds(100);
    // lcd_device_struct->lcd_send_commandbyte(CLEAR_DISPLAY);
    // lcd_device_struct->delay_mseconds(100);

    return 0;

}

void lcd_clear_display(struct char_lcd_dev *lcd_device_struct)
{
    lcd_device_struct->lcd_send_commandbyte(CLEAR_DISPLAY);

}
void lcd_goto_home(struct char_lcd_dev *lcd_device_struct)
{
    lcd_device_struct->lcd_send_commandbyte(RETURN_HOME);


}

void lcd_goto(uint8_t row, uint8_t column, struct char_lcd_dev *lcd_device_struct)
{
#ifdef LCD_16x2
    lcd_device_struct->lcd_send_commandbyte(SET_CURSOR_BASE | (0x00 + ( (row % 2) * 64) ) | column);
#endif
}

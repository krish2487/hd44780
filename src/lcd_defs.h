#ifndef __LCD_DEFS
#define __LCD_DEFS

#include <stdint.h>

#define CLEAR_DISPLAY 0x01

#define RETURN_HOME 0x02

#define ENTRY_MODE_BASE 0x04
#define ENTRY_MODE_INCREMENT (1 << 1)
#define ENTRY_MODE_DECREMENT (0 << 1)
#define ENTRY_MODE_DISPLAYSHIFT (1)
#define ENTRY_MODE_NODISPLAYSHIFT (0)

#define DISPLAY_CONTROL_BASE 0x08
#define DISPLAY_CONTROL_ON (1 << 2)
#define DISPLAY_CONTROL_OFF (1 << 2)
#define DISPLAY_CONTROL_CURSORON (1 << 1)
#define DISPLAY_CONTROL_CURSOROFF (0 << 1)
#define DISPLAY_CONTROL_BLINK (1)
#define DISPLAY_CONTROL_NOBLINK (0)

#define DISPLAY_SHIFT_BASE 0x10
#define DISPLAY_SHIFT_CURSOR (0 << 3)
#define DISPLAY_SHIFT_DISPLAY (1 << 3)
#define DISPLAY_SHIFT_RIGHT (1 << 2 )
#define DISPLAY_SHIFT_LEFT (0 << 2 )

#define FUNCTION_SET_BASE 0x20
#define FUNCTION_SET_DL_8BIT (1 << 4)
#define FUNCTION_SET_DL_4BIT (0 << 4)
#define FUNCTION_SET_1_LINE (0 << 3)
#define FUNCTION_SET_2_LINE (1 << 3)
#define FUNCTION_SET_5x11 (1 << 2)
#define FUNCTION_SET_5x8 (0 << 2)

#define SET_CURSOR_BASE 0x80

typedef void (*command_ptr)(uint8_t byte);
typedef void (*data_ptr)(char byte);
typedef void (*delay_ptr)(uint16_t delay);

struct char_lcd_dev
{

    uint8_t entry_mode;
    uint8_t display_control;
    uint8_t display_shift;
    uint8_t function_set;

    data_ptr lcd_send_databyte;
    command_ptr lcd_send_commandbyte;
    delay_ptr delay_useconds;
    delay_ptr delay_mseconds;

};


#endif

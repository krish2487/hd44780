#ifndef LCD_H
#define LCD_H


#include "lcd_defs.h"
#include "delay.h"
#include <stdint.h>

//Different types of lcd displays
#define LCD_16x2
// #define LCD_20x4
// #define LCD_8x2
// #define LCD_20x2

int lcd_init(struct char_lcd_dev *lcd_device_struct);
void lcd_clear_display(struct char_lcd_dev *lcd_device_struct);
void lcd_goto_home(struct char_lcd_dev *lcd_device_struct);
void lcd_goto(uint8_t row, uint8_t column, struct char_lcd_dev *lcd_device_struct);
void lcd_send_data(char *data, uint8_t length, struct char_lcd_dev *lcd_device_struct);
struct char_lcd_dev *init_lcd_struct(uint8_t entry_mode, uint8_t display_control, uint8_t function_set, uint8_t display_shift, data_ptr send_data, command_ptr send_command, delay_ptr milliseconds, delay_ptr microseconds);

#endif
#include "hd44780.h"
#include "bsp.h"

int8_t lcd_init(struct char_lcd_dev *dut_struct)
{
    if ( dut_struct->entry_mode >= ENTRY_MODE_BASE)
        {
            return -1;
        }
    if (dut_struct->display_control >= DISPLAY_CONTROL_BASE)
        {
            return -1;
        }

    dut_struct->lcd_send_commandbyte(DISPLAY_CONTROL_BASE | DISPLAY_CONTROL_OFF);
    return 0;
}
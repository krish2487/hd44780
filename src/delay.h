#pragma once

#ifdef __cplusplus
extern "C" {
#endif

    /**
     * Initialize the timers used for delays.
     */
void setup_delay_timer(void);

void delay_us(uint16_t delay);

void delay_ms(uint16_t delay);



#ifdef __cplusplus
}
#endif
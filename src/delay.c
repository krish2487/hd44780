#include <stdint.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>
#include "libopencm3/cm3/systick.h"
#include "delay.h"


void setup_delay_timer(void)
{
    //Enable Timer2 Peripheral clock
    rcc_periph_clock_enable(RCC_TIM6);
    //Setup Timer 2, no clock division, edge aligned, up counting
    // timer_set_mode(TIM6, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
    //RCC_CLOCK_VRANGE1_HSI_RAW_16MHZ - APB1 is 16Mhz, Prescaler of 16
    timer_set_prescaler(TIM6, rcc_apb1_frequency / (1e6 -1));
    // //Enable IRQ for TIM2
    // nvic_enable_irq(NVIC_TIM6_IRQ);
    // timer_clear_flag(TIM6, TIM_CR1_URS);
    timer_set_period(TIM6, 0xffff);
    timer_one_shot_mode(TIM6);

}

void delay_us(uint16_t delay)
{
    // timer_set_period(TIM6, delay);
    // //Set the start value for the timer2
    // timer_set_counter(TIM6, 0);
    // //Enable the timer
    // timer_enable_counter(TIM6);

    // while(timer_get_flag(TIM6, TIM_SR_UIF)!= 1){
    // }
    // timer_clear_flag(TIM6, TIM_SR_UIF);
    // timer_disable_counter(TIM6);
    setup_delay_timer();
    //The divide by 2.64 gives correct delay empirically. maybe due to tolerance in the internal oscillator
    TIM_ARR(TIM6) = delay ;
    // TIM_ARR(TIM6) = (uint16_t) delay/2.545 ;
    TIM_EGR(TIM6) = TIM_EGR_UG;
    TIM_CR1(TIM6) |= TIM_CR1_CEN;
    while(TIM_CR1(TIM6) & TIM_CR1_CEN);

}


void delay_ms(uint16_t delay)
{
    // // setup_delay_timer();
    // // Timer period is set to 1000 gives us a 1 hz rate of interrupt
    // timer_set_period(TIM6, delay*1000);
    // //Set the start value for the timer2
    // timer_set_counter(TIM6, 0);
    // //Enable the timer
    // timer_enable_counter(TIM6);
    // while(timer_get_flag(TIM6, TIM_SR_UIF)!=1){
    // }
    // timer_clear_flag(TIM6, TIM_SR_UIF);
    // timer_disable_counter(TIM6);
    setup_delay_timer();
    for (int i = 0; i <= delay; ++i)
    {
        delay_us(1000);
        /* code */
    }
}

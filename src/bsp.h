#ifndef __BSP__H
#define __BSP__H
#include <stdint.h>

void lcd_command(uint8_t byte);
void lcd_data(char byte);
void delay(uint16_t delay);


#endif
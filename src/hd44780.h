#ifndef _HD44780_H
#define _HD44780_H
#include "lcd_defs.h"
#include "stdlib.h"
#include "stdbool.h"

int8_t lcd_init(struct char_lcd_dev *dut_struct);

#endif // _HD44780_H
